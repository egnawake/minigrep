use std::env;

pub struct Config {
    pub query: String,
    pub file_path: String,
    pub ignore_case: bool,
}

impl Config {
    pub fn build(
        mut args: impl Iterator<Item = String>
    ) -> Result<Config, &'static str> {
        let mut ignore_case = env::var("IGNORE_CASE").is_ok();

        // Skip program name
        args.next();

        let first_required = loop {
            if let Some(arg) = args.next() {
                if !arg.starts_with('-') {
                    break Some(arg);
                }
                if arg == "-i" {
                    ignore_case = true;
                }
            } else {
                break None;
            }
        };

        let query = match first_required {
            Some(s) => s,
            None => return Err("missing query"),
        };

        let file_path = match args.next() {
            Some(s) => s,
            None => return Err("missing file path"),
        };

        Ok(Config {
            query,
            file_path,
            ignore_case,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn required_args() {
        let query = "abc".to_string();
        let file_path = "file.txt".to_string();

        let args = vec![String::new(), query, file_path];
        let config = Config::build(&args).unwrap();

        assert_eq!("abc", config.query);
        assert_eq!("file.txt", config.file_path);
        assert_eq!(false, config.ignore_case);
    }

    #[test]
    fn ignore_case_arg() {
        let query = "abc".to_string();
        let file_path = "file.txt".to_string();

        let args = vec![String::new(), "-i".to_string(), query, file_path];
        let config = Config::build(&args).unwrap();

        assert_eq!("abc", config.query);
        assert_eq!("file.txt", config.file_path);
        assert_eq!(true, config.ignore_case);
    }
}

